/**
 * Created by austin on 04/11/2016.
 */
//Building the Sever
var express = require('express');
var mongoose = require('mongoose');

// Added to parse json files that are posted or put
var bodyParser = require('body-parser');

var app = express();

// Add middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/emberData');

//set the appropriate headers for our server.
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  next();
});

//Creating our Mongoose Model
var taskSchema = new mongoose.Schema({
  task: {
    title: 'string',
    description: 'string',
    date: 'date',
    created: 'date',
  },
});

var messageSchema = new mongoose.Schema({
  message: 'string',
  country: 'string',
  country2: 'string',
  name: 'string',
  email: 'string',
});

//defining or schema
var TaskModel = mongoose.model('task', taskSchema);
var MessageModel = mongoose.model('message', messageSchema);

//set up route
app.get('/api/', function (req, res) {
  res.send('Working');
});

// route to get all tasks
app.get('/api/tasks', function (req, res) {
  TaskModel.find({}, function (err, docs) {
    if (err) {
      res.send({ Error: err });
    } else {
      res.send({ task: docs });
    }
  });
});

app.post('/api/tasks', function (req, res, next) {
  console.log(req.body);
  TaskModel.create(req.body, function (err, post) {
    if (err) return next(err);
    res.status(200).json(post);
  });
});

app.post('/api/message', function (req, res, next) {
  console.log(req.body);
  MessageModel.create(req.body, function (err, post) {
    if (err) return next(err);
    res.status(200).json(post);
  });
});

app.listen('4500');
